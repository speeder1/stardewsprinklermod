﻿/*
    Copyright 2016 Maurício Gomes (Speeder)

    Storm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Storm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Storm.  If not, see <http://www.gnu.org/licenses/>.
 */

using Storm.ExternalEvent;
using Storm.StardewValley.Wrapper;
using Storm.StardewValley.Event;

namespace SprinklerMod
{
    [Mod]
    public class SprinklerMod : DiskResource
    {
        [Subscribe]
        public void BeforeObjectDayUpdateCallback(BeforeObjectDayUpdateEvent @event)
        {
            ObjectItem obj = @event.This;
            GameLocation location = @event.ArgLocation;
            if (obj.Name == "Sprinkler")
            {
                Microsoft.Xna.Framework.Vector2 tileLocation = obj.TileLocation;
                tileLocation.X -= 2;
                int counter = 0;

                while(counter < 5)
                {                    
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;                                                
                    }
                    tileLocation.X += 1;
                    ++counter;
                }

                tileLocation.X -= 3;
                tileLocation.Y -= 2;
                counter = 0;

                while (counter < 5)
                {
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;
                    }
                    tileLocation.Y += 1;
                    ++counter;
                }
                @event.ReturnEarly = true;
            }

            if (obj.Name == "Quality Sprinkler")
            {
                Microsoft.Xna.Framework.Vector2 tileLocation = obj.TileLocation;
                tileLocation.X -= 3;
                int counter = 0;

                while (counter < 7)
                {
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;
                    }
                    tileLocation.X += 1;
                    ++counter;
                }

                tileLocation.X -= 4;
                tileLocation.Y -= 3;
                counter = 0;

                while (counter < 7)
                {
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;
                    }
                    tileLocation.Y += 1;
                    ++counter;
                }

                tileLocation.X -= 1;
                tileLocation.Y -= 5;
                float xEnd = tileLocation.X + 3;
                float yEnd = tileLocation.Y + 3;                

                while(tileLocation.X < xEnd)
                {
                    while(tileLocation.Y < yEnd)
                    {
                        if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                        {
                            HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                            hoeDirtObj.State = 1;
                        }
                        tileLocation.Y += 1;
                    }
                    tileLocation.Y -= 3;
                    tileLocation.X += 1;
                }


                @event.ReturnEarly = true;
            }

            if (obj.Name == "Iridium Sprinkler")
            {
                Microsoft.Xna.Framework.Vector2 tileLocation = obj.TileLocation;
                tileLocation.X -= 6;
                int counter = 0;

                while (counter < 13)
                {
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;
                    }
                    tileLocation.X += 1;
                    ++counter;
                }

                tileLocation.X -= 7;
                tileLocation.Y -= 6;
                counter = 0;

                while (counter < 13)
                {
                    if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                    {
                        HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                        hoeDirtObj.State = 1;
                    }
                    tileLocation.Y += 1;
                    ++counter;
                }

                tileLocation.X -= 2;
                tileLocation.Y -= 9;
                float xEnd = tileLocation.X + 5;
                float yEnd = tileLocation.Y + 5;
                

                while (tileLocation.X < xEnd)
                {
                    while (tileLocation.Y < yEnd)
                    {
                        if (location.TerrainFeatures.ContainsKey(tileLocation) && location.TerrainFeatures[tileLocation].IsHoeDirt())
                        {
                            HoeDirt hoeDirtObj = location.TerrainFeatures[tileLocation].AsHoeDirt();
                            hoeDirtObj.State = 1;
                        }
                        tileLocation.Y += 1;
                    }
                    tileLocation.Y -= 5;
                    tileLocation.X += 1;
                }
                @event.ReturnEarly = true;
            }
        }
    }
}
